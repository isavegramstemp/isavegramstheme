<?php
/*
* @package RedInstaBot
* @version 2.0.0
* @author by Mr Red
* @author telegram @redznore
* @author skype red_na_bmwx5
*/

//===========================================

	ignore_user_abort(true);
	set_time_limit(0);
	// ini_set('display_errors', 1);
	// error_reporting(E_ALL);
	ini_set('memory_limit', '2048M');
	
//===========================================

// init
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rsd_link');

add_filter( 'term_description', 'shortcode_unautop');
add_filter( 'term_description', 'do_shortcode' );

add_filter('widget_text', 'do_shortcode');
add_theme_support('post-thumbnails');

register_nav_menu('header', 'Хедер меню');
register_nav_menu('footer', 'Нижнее меню');

if( function_exists('acf_add_options_page') ){
	acf_add_options_page( [
		'page_title'	=> 'Theme settings',
	] );
}

//===========================================

add_action('wp_enqueue_scripts', function(){
	wp_enqueue_style('style', get_stylesheet_directory_uri() . '/style.css', false, '1.0', 'all');
	wp_enqueue_script('front', get_stylesheet_directory_uri() . '/js/common.js', ['jquery'], false, true );
	
	wp_localize_script('front', 'insta', 
			[
				'ajax'	=> admin_url('admin-ajax.php'),
			]
		);
});
		

//===========================================
add_action('wp_ajax_gallery',				'get_gallery');
add_action('wp_ajax_nopriv_gallery',		'get_gallery');

function get_gallery(){
	include __DIR__ . '/tpl/tpl-posts.php';
	
	if( ( isset( $_REQUEST['action'] ) ) == 'gallery' )
		wp_die();
}

add_action('wp_ajax_download',				'download');
add_action('wp_ajax_nopriv_download',		'download');

function download(){
	$file = $_REQUEST['file'];
	
	header('Content-Type: application/octet-stream');
	header('Content-Disposition: attachment; filename=download.jpg');
    readfile( $file );
	exit;
}

add_action('wp_ajax_search',				'search');
add_action('wp_ajax_nopriv_search',			'search');

function search(){
	global $wpdb;
	
	if( isset( $_REQUEST['s'] ) ){
		$s = preg_replace('/[^a-zA-Z0-9_-]/i', '',  $_REQUEST['s'] );
		
		$data['users']	= $wpdb->get_results('SELECT p.post_title as name, p.post_name as slug, m.`meta_value` FROM `'. $wpdb->posts .'` as p LEFT JOIN `'. $wpdb->postmeta .'` as m ON p.`ID` = m.`post_id` AND m.`meta_key`="cache" WHERE m.`meta_value` != "" AND p.`post_title` LIKE "%'. $s .'%" LIMIT 5');
		$data['tags']	= $wpdb->get_results('SELECT name, slug  FROM `'. $wpdb->terms .'` WHERE `name` LIKE "%'. $s .'%" LIMIT 5');
		
		if( $data ){ $html = '';
			foreach($data as $key => $items){
				foreach($items as $item){
				
					$meta = @ $item->meta_value ? maybe_unserialize( $item->meta_value ) : '';
				
					$html .= '<div class="item_result_search '.( $key == 'tags' ? 'item_hashtag' : '' ).'">
								<a href="'.( $key == 'tags' ? '/tags' : '' ).'/'. $item->slug .'/" class="image_story">
									'.( isset( $meta->entry_data->ProfilePage[0]->graphql->user->profile_pic_url ) ? '<img src="'. $meta->entry_data->ProfilePage[0]->graphql->user->profile_pic_url .'" />' : '' ).'
								</a>
								<div class="right_item">
									<a href="'.( $key == 'tags' ? '/tags' : '' ).'/'. $item->slug .'/" class="name_item_result '.( isset( $meta->entry_data->ProfilePage[0]->graphql->user->is_verified ) ? 'verify_item' : '' ).'">'. $item->name .'</a>
									<div class="descr_item_resutl">'.( isset( $meta->entry_data->ProfilePage[0]->graphql->user->full_name ) ? $meta->entry_data->ProfilePage[0]->graphql->user->full_name : '' ).'</div>
								</div>
							</div>';
				}
			}
			wp_die( $html );
		}
	}
}

function _get_meta( $post ){
	$meta = new stdClass;
	
	foreach( (array) get_post_meta( $post->ID ) as $k => $v )
		$meta->$k = @ maybe_unserialize( $v[0] );
		
	$meta->slug		= $post->post_name;
	$meta->username	= get_the_title( $post->post_parent );
	$meta->sluguser	= get_the_permalink( $post->post_parent );
	$meta->time		= date('d.m.Y H:i', strtotime( $post->post_date ) );
	$meta->content	= $post->post_content;
	$meta->profile_pic_url	= get_post_meta( $post->post_parent, 'profile_pic_url', true );
		
	return $meta;
}

//===========================================
	if ( ! function_exists('post_exists') ) {
		require_once( ABSPATH . 'wp-admin/includes/post.php' );
		// require_once( ABSPATH . '/wp-admin/includes/taxonomy.php');
	}

	require dirname(__FILE__) . '/libs/mcurl.php';
	require dirname(__FILE__) . '/libs/phpquery.php';
	require dirname(__FILE__) . '/insta.class.php';
	
//===========================================

new RedInstaBot();

?>