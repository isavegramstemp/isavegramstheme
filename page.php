<?php get_header(); ?>
	<?php if (have_posts()) { ?>
	
		<?php while (have_posts()) { the_post(); ?>
			
			<div class="section_text">
				<div class="container">
					<div class="title_min"><span><?php the_title() ?></span></div>
					
					<?php the_content() ?>
					
				</div>
			</div>
			
		<?php } ?>
	<?php } ?>
	
<?php get_footer(); ?>