<?php 
	get_header(); 
	
	$args = [
				'post_type'			=> 'post', 
				'post_status'		=> 'publish', 
				// 'orderby'			=> 'rand', 
				'orderby'			=> 'meta_value',
				'order'				=> 'DESC',
				'meta_key'			=> 'edge_followed_by',
				'posts_per_page'	=> 24,
			];
	
	$users = get_posts( $args );

?>
		<div class="section_top">
			<div class="container">
				<div class="title_min">Top Instagram Users</div>
				<div class="row wrap_users">
					
					<?php if( $users ){ ?>
						<?php foreach( $users as $user ){ ?>
							<div class="col-lg-3 col-4">
								<div class="item_user">
									<a href="<?=get_the_permalink( $user->ID )?>" class="image_item_user">
										<img src="<?=get_post_meta($user->ID, 'profile_pic_url_hd', true )?>" alt="<?=$user->post_title?>">
									</a>
									<div class="content_item_user">
										<a href="<?=get_the_permalink( $user->ID )?>" class="name_item_user"><?=$user->post_title?></a>
										<br>
										<a href="<?=get_the_permalink( $user->ID )?>" class="link_main">(@<?=$user->post_title?>)</a>
										<br>
										<div class="descr_item_user"><?=get_post_meta($user->ID, 'edge_followed_by', true )->count?> follovers</div>
									</div>
								</div>
							</div>
						<?php }?>
					<?php }?>
					
					
				</div>
				
				<div class="align_center">
					<a href="#" class="link_main link_more">More</a>
				</div>
				<div class="line hidden_tablet"></div>
			</div>
		</div>
		
		<div class="bottom_text hidden_tablet">
			<div class="container">
				<?=term_description($cat, 'category') ?>	
			</div>
		</div>

<?php get_footer(); ?>