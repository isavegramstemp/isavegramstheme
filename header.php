<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ru" class="lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html lang="ru" class="lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html lang="ru" class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">
<!--<![endif]-->
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		
		<title><?php if (is_home()) { echo bloginfo('description');} elseif (is_front_page()) { wp_title(''); } else { wp_title(''); } ?></title>
		<?php wp_head(); ?>
		
		<link rel="shortcut icon" href="favicon.png" />
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/libs/bootstrap/bootstrap-grid.css" />
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/libs/font-awesome-4.2.0/css/font-awesome.min.css" />
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/libs/fancybox/jquery.fancybox.css" />
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/libs/slick-1.5.9/slick/slick.css" />
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/libs/slick-1.5.9/slick/slick-theme.css" />
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/fonts.css" />
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/main.css?ver=1.2" />
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/media.css?ver=1.2" />
	</head>
<body>
	<div class="wrapper">
		
		<header class="main_header">
			<div class="container">
				<div class="d_flex">
					<a href="/" class="main_logo">
						
						<img src="<?=get_field('logo', 'option')['url'] ?>" alt="alt">
					</a>
					<a href="search.html" class="btn_search_tablet"></a>
					<form class="form_search">
						<button class="btn_search"></button>
						<input type="search" placeholder="Search" required="required">
						<div class="clear_search"></div>
						
						<div class="result_search">
							<div class="result_wrap_list" id="result_search">
							</div>
						</div>
							
					</form>
					<div class="top_users_header">
						<a href="/top-instagram-users/" class="link_main">Top Instagram Users</a>
						
						<div class="rating_list">
							<?php if( function_exists('the_ratings') ) the_ratings(); ?>
						</div>
					</div>
					<div class="share_block">
						<p>Share this:</p>
						<ul class="share_list">
							<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/tg.svg" alt="alt"></a></li>
							<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/in.svg" alt="alt"></a></li>
							<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/tw.svg" alt="alt"></a></li>
							<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/fb.svg" alt="alt"></a></li>
							<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/wp.svg" alt="alt"></a></li>
						</ul>
					</div>
					<a href="#" class="btn_social"></a>
					<ul class="social_list_mob">
						<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/tg.svg" alt="alt"><span>Telegram</span></a></li>
						<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/in.svg" alt="alt"><span>LinkedIn</span></a></li>
						<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/tw.svg" alt="alt"><span>Twitter</span></a></li>
						<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/fb.svg" alt="alt"><span>Facebook</span></a></li>
						<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/wp.svg" alt="alt"><span>WhatsApp</span></a></li>
					</ul>
				</div>
			</div>
		</header>

		<?php if( ! is_404() ){ ?>
			<?php if( $banner = get_field('banner_header', 'option') ){ ?>
				<div class="main_billbord">
					<div class="container">
						<?=$banner ?>
					</div>
				</div>
			<?php } ?>
		<?php } ?>
		
		<div class="container">
			<div class="line hidden_tablet"></div>
		</div>