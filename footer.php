		<?php if( ! is_404() ){ ?>
			<?php if( $banner = get_field('banner_footer', 'option') ){ ?>				
				<div class="main_billbord bottom_billbord">
					<div class="container">
						<div class="line hidden_desktop"></div>
					</div>
				
					<div class="container">
						<?=$banner ?>	
					</div>
				</div>
			<?php } ?>
			<br />
		<?php } ?>

		<?php if( $_SERVER['REQUEST_URI'] == '/' ){ ?>
			<div class="bottom_text hidden_tablet">
				<div class="container">
					<?=get_field('home_desc', 'option') ?>	
				</div>
			</div>
		<?php } ?>
		
		<footer class="main_footer">
			<div class="container">
				<div class="line hidden_tablet"></div>
				
				<?php
					$args	= [
								'menu'				=> 'footer',
								'container'			=> '',
								'container_class'	=> '',
								'container_id'		=> '',
								'echo'				=> 0,
								'items_wrap'		=> '<ul class="nav_footer">%3$s</ul>',
								'depth'				=> 0,
							];
					$menu = wp_nav_menu( $args );
					
					echo str_replace('<a', '<a class="link_main"', $menu);
				?>
				
				
			</div>
		</footer>

		<div class="footer_mobile">
			<div class="container">
				<div class="d_flex">
					<a href="search.html" class="btn_search_tablet"></a>
					<div class="top_users_header">
						<a href="#" class="link_main">Top Instagram Users</a>
						<ul class="rating_list">
							<li><div class="rating_item active"></div></li>
							<li><div class="rating_item active"></div></li>
							<li><div class="rating_item active"></div></li>
							<li><div class="rating_item active"></div></li>
							<li><div class="rating_item"></div></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<?php get_template_part('tpl/photos-modal')?>
	<?php get_template_part('tpl/video-modal')?>
	

		<!--[if lt IE 9]>
			<script src="<?php bloginfo('template_url'); ?>/libs/html5shiv/es5-shim.min.js"></script>
			<script src="<?php bloginfo('template_url'); ?>/libs/html5shiv/html5shiv.min.js"></script>
			<script src="<?php bloginfo('template_url'); ?>/libs/html5shiv/html5shiv-printshiv.min.js"></script>
			<script src="<?php bloginfo('template_url'); ?>/libs/respond/respond.min.js"></script>
		<![endif]-->
		<script src="<?php bloginfo('template_url'); ?>/libs/jquery/jquery-1.11.1.min.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/libs/jquery-mousewheel/jquery.mousewheel.min.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/libs/bootstrap/bootstrap.min.js"></script>	
		<script src="<?php bloginfo('template_url'); ?>/libs/fancybox/jquery.fancybox.pack.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/libs/slick-1.5.9/slick/slick.min.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/libs/scroll2id/PageScroll2id.min.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/libs/jQueryFormStyler-master/jquery.formstyler.min.js"></script>
			
		<?php wp_footer(); ?>
		
	

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158793197-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158793197-1');
</script>
	
	
<span style="display: none;">
<!--LiveInternet counter--><script type="text/javascript">
document.write('<a href="//www.liveinternet.ru/click" '+
'target="_blank"><img src="//counter.yadro.ru/hit?t26.5;r'+
escape(document.referrer)+((typeof(screen)=='undefined')?'':
';s'+screen.width+'*'+screen.height+'*'+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+';u'+escape(document.URL)+
';h'+escape(document.title.substring(0,150))+';'+Math.random()+
'" alt="" title="LiveInternet: показано число посетителей за'+
' сегодня" '+
'border="0" width="88" height="15"><\/a>')
</script><!--/LiveInternet-->
</span>	


<span style="display: none;">
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(57635086, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<?php if(false):?>
<noscript><div><img src="https://mc.yandex.ru/watch/57635086" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<?php endif;?>
<!-- /Yandex.Metrika counter -->
</span>	
		
	</body>
</html>