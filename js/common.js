$(document).ready(function() {
	
		var url = window.location.pathname;
		var title = $('title').text();

		$(document).on('input', 'input[type=search]', function(){
			
			var s = $(this).val();
			
			if( s.length < 1 ){
				$('#result_search').html('');
				return;
			}
			
			$.ajax({
				type: 'POST',
				// dataType: 'json',
				url: insta.ajax,
				data: {s: s, action: 'search'},
				success: function( data ){
					
					$('#result_search').html( data );
					
					// for(var i in data){
						// $('#result_search').append();
				}
			});
			
		});

		$('#load-more').click(function(){
			var e = $(this);
			e.text('...');
			
			$.ajax({
				type: 'POST',
				url: insta.ajax,
				data: { action: 'gallery' },
				success: function(data){
					e.text('More');
					$('#load-more-data').append( data );
				},
				error:  function(xhr, str){
					console.log( 'Error: '+ xhr );
				}
			});
			
			return false;
		});
	
	//=======================================================================
	$(".form_search input").click(function(){
		$(".form_search").addClass("active");
	});
	$(".close_search").click(function(){
		$(".form_search").removeClass("active");
	});

	$(".form_search_page input").click(function(){
		$(".form_search_page").addClass("active");
	});
	$(".clear_input").click(function(){
		$(".form_search_page").removeClass("active");
		$(".result_search_page").fadeOut(200);
			$(".recent_result").fadeIn(0);
			var val3 = $('.form_search_page input').val();
		if(val3.length >= 1){
			$('.form_search_page input').val('');
		}
	});

	$(document).mouseup(function (e) {
		var container = $(".form_search");
		if (container.has(e.target).length === 0){
			container.removeClass("active");
			$('.form_search input').val('');
			$(".result_search").fadeOut(200);
		}
	});


	$('.clear_search').on('click', function(e){
		e.preventDefault();
		$(".form_search").removeClass("active");
		$(".result_search").fadeOut(200);
		var val = $('.form_search input').val();
		if(val.length >= 1){
			$('.form_search input').val('');
		}
	});


	$('.form_search input').on('keyup blur', function () {
		var val1 = $('.form_search input').val();
		if(val1.length >= 1){
			$(".result_search").fadeIn(200);
		}
	}); 

	$('.form_search_page input').on('keyup blur', function () {
		var val2 = $('.form_search_page input').val();
		if(val2.length >= 1){
			$(".result_search_page").fadeIn(200);
			$(".recent_result").fadeOut(0);
		} else {
			$(".result_search_page").fadeOut(200);
			$(".recent_result").fadeIn(0);
		}
	}); 


	$('.close_btn_modal').click(function (e) {
		e.preventDefault();
		$.fancybox.close();
		history.replaceState( { title: title, url: url }, title, url );
	});
	$('.close_btn_mobile').click(function (e) {
		e.preventDefault();
		$.fancybox.close();
		history.replaceState( { title: title, url: url }, title, url );
	});
	$('.clear_item').click(function (e) {
		e.preventDefault();
		$(this).parent().fadeOut(200);
	});

	/*высота меню*/
    function heightDetect() {
    $('.wrapper_result').css("height", $(window).height() - $(".main_header").height() - $(".main_billbord").height() - $(".form_search_page").height() - $(".main_footer").height() - 400);
  };
    heightDetect();
    $(window).resize(function() {
      heightDetect();
    });


	/*видео в слайдере*/
	$('.modal_wrap_video .slider_main').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
		var current = $(slick.$slides[currentSlide]);
		current.html(current.html());
	});
	$('.modal_wrap_video .slider_modal').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
		var current = $(slick.$slides[currentSlide]);
		current.html(current.html());
	});

	//кнопка sandwich
	$(".btn_nav").click(function() {
		$(".sandwich").toggleClass("active");
		if ($(".menu").is(":hidden")) {
			$(".menu").slideDown(600);
		} else {
			$(".menu").slideUp(600);
		}
		
	});

	$(".menu a").click(function() {
		$(".menu").slideUp(600);
		$(".sandwich").removeClass("active");
	});

	$(".btn_social").click(function(e) {
		e.preventDefault();
		if ($(this).siblings(".social_list_mob").is(":hidden")) {
			$(this).siblings(".social_list_mob").fadeIn(200);
		} else {
			$(this).siblings(".social_list_mob").fadeOut(200);
		}
	});

	$(".social_list_mob a").click(function() {
		$(".social_list_mob").fadeOut(200);
	});


	$(document).mouseup(function (e){ 
			var div = $(".social_list_mob"); 
			if (!div.is(e.target) 
				&& div.has(e.target).length === 0) { 
				div.fadeOut(200); 
		}
	});
/* 
	$(document).on('click', 'a[href=#photos_modal1], a[href=#video_modal1]', function(){
		
		e = $(this).attr('href');
		item = $(this).closest('.item_gallery').data('item');
		
		history.replaceState( { title: item.content, url: '/p/'+ item.slug +'/' }, item.content, '/p/'+item.slug +'/' );
		
		if( e == '#video_modal1' ){
			slide = '<li><div class="item_slide"><video width="100%" height="768px" controls="controls" poster="'+ item.video_url +'"><source src="'+ item.video_url +'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'></video><a href="#" class="download_btn"></a></div></li>';
		} else {
			slide = '<li><div class="item_slide"><img src="'+ item.thumbnail_resources[ item.thumbnail_resources.length - 1 ].src +'" alt="alt"><a href="#" class="download_btn"></a></div></li>';
		}
			
		$( e ).find('.username').text( item.username );
		$( e ).find('.profile_pic_url').attr('src', item.profile_pic_url);
		$( e ).find('.slider_main').html('').append( slide );
		$( e ).find('#content').html( item.content );
		
		console.log( item );
		console.log( item.thumbnail_resources[3].src );
	});
	 */		
	$('.fancybox').fancybox({
		beforeShow: function(){
			
			var e = $.fancybox.current.element;
			a = $( e ).attr('href');
			item = $( e ).closest('.item_gallery, .item_story').data('item');
			if( $( e ).closest('.item_story').length ){
			
				if( item.data.reels_media[0].items ){
					slide = '';
					for(i=0; i< item.data.reels_media[0].items.length; i++  ){
						story = item.data.reels_media[0].items[ i ].display_resources[ item.data.reels_media[0].items[ i ].display_resources.length - 1 ].src;
						slide += '<li><div class="item_slide"><img src="'+ story +'" alt="alt"><a href="'+ insta.ajax +'?action=download&file='+ encodeURIComponent( story ) +'" class="download_btn"></a></div></li>';
					}
					$( a ).find('.image_story').attr('href', item.sluguser);
					$( a ).find('.avatar_modal').attr('href', item.sluguser);
					$( a ).find('.username').text( item.data.reels_media[0].owner.username ).attr('href', '');
					$( a ).find('.profile_pic_url').attr('src', item.data.reels_media[0].owner.profile_pic_url);
					$( a ).find('.slider_main').html('').append( slide );
					$( a ).find('#content').html('');
				}
				
				console.log( item );
			
			} else {
				history.replaceState( { title: item.content, url: '/p/'+ item.slug +'/' }, item.content, '/p/'+item.slug +'/' );
				var slide = '';
				if( a == '#video_modal1' ){
					slide = '<li><div class="item_slide"><video width="100%" height="768px" controls="controls" poster="'+ item.video_url +'"><source src="'+ item.video_url +'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'></video><a href="'+ encodeURIComponent( item.video_url ) +'" class="download_btn"></a></div></li>';
				} else if(item.edge_sidecar_to_children) {
					for(i=0; i< item.edge_sidecar_to_children.length; i++  ){
						src = item.edge_sidecar_to_children[i][item.edge_sidecar_to_children[i].length -1].src;
						slide += '<li><div class="item_slide"><img src="'+ src +'" alt="alt"><a href="'+ insta.ajax +'?action=download&file='+ encodeURIComponent( src ) +'" class="download_btn"></a></div></li>';
					}
				}
				else{
					console.log(item);
					slide = '<li><div class="item_slide"><img src="'+ item.thumbnail_resources[ item.thumbnail_resources.length - 1 ].src +'" alt="alt"><a href="'+ insta.ajax +'?action=download&file='+ encodeURIComponent( item.thumbnail_resources[ item.thumbnail_resources.length - 1 ].src ) +'" class="download_btn"></a></div></li>';
				}
				$( a ).find('.avatar_modal').attr('href', item.sluguser);
				$( a ).find('.image_story').attr('href', item.sluguser);
				$( a ).find('.time').text( item.time );
				$( a ).find('.username').text( item.username ).attr('href', item.sluguser);
				$( a ).find('.profile_pic_url').attr('src', item.profile_pic_url);
				$( a ).find('.slider_main').html('').append( slide );
				$( a ).find('#content').html( item.content );
			}
			
			$('.slider_modal').slick('setPosition');
			$('.slider_main').slick('refresh');
			
		},
		afterClose: function(){
			history.replaceState( { title: title, url: url }, title, url );
		}
	});
	
	$(".slider_modal").slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		swipe: false,
		arrows: true,
		fade: true,
		dots: true,
		responsive: [{
			breakpoint: 967,
			settings: 'unslick'
		}]
	});
	$(".slider_main").not('.slick-initialized').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		dots: true,
		autoplay: true,
  		autoplaySpeed: 5000,
	});


});