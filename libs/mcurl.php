<?php

class AngryCurl extends RollingCurl {
    public static $debug_info       =   array();
    public static $debug_log        =   false;
    protected static $console_mode  =   false;
    
    
    protected static $array_alive_proxy=array();
    protected $array_proxy          =   array();
    protected $array_url            =   array();
    protected $array_useragent      =   array();
    
    protected $error_limit          =   0;
    protected $array_valid_http_code=   array(200);
    
    protected $n_proxy              =   0;
    protected $n_useragent          =   0;
    protected $n_url                =   0;
    
    protected $proxy_test_url       =   'http://google.com';
    protected static $proxy_valid_regexp   =   '';
    
    private $use_proxy_list       =   false;
    private $use_useragent_list   =   false;
    

    function __construct($callback = null, $debug_log = false)
    {
        self::$debug_log = $debug_log;
        self::add_debug_msg("# Building");
        
        if(!function_exists('curl_init'))
        {
            throw new AngryCurlException("(!) cURL is not enabled");
        }
        
        parent::__construct($callback);
    }

    public function init_console()
    {
        self::$console_mode = true;
        
        echo "<pre>";
        
        if (function_exists('apache_setenv'))
        {
            @apache_setenv('no-gzip', 1);
        }
        @ini_set('zlib.output_compression', 0);
        @ini_set('implicit_flush', 1);
        for ($i = 0; $i < ob_get_level(); $i++)
            ob_end_flush();
        ob_implicit_flush(1);
        
        self::add_debug_msg("# Console mode activated");
    }

    public function request($url, $method = "GET", $post_data = null, $headers = null, $options = null)
    {
        if($this->n_proxy > 0 && $this->use_proxy_list)
        {
            $options[CURLOPT_PROXY]=$this->array_proxy[ mt_rand(0, $this->n_proxy-1) ];
        }
        elseif($this->n_proxy < 1 && $this->use_proxy_list)
        {
            throw new AngryCurlException("(!) Option 'use_proxy_list' is set, but no alive proxy available");
        }
        
        if($this->n_useragent > 0 && $this->use_useragent_list)
        {
            $options[CURLOPT_USERAGENT]=$this->array_useragent[ mt_rand(0, $this->n_useragent-1) ];
        }
        elseif($this->n_useragent < 1 && $this->use_useragent_list)
        {
            throw new AngryCurlException("(!) Option 'use_useragent_list' is set, but no useragents available");
        }

        parent::request($url, $method, $post_data, $headers, $options);
        return true;
    }
    
    public function execute($window_size = null)
    {
        if($window_size == null)
        {
            self::add_debug_msg(" (!) Default threads amount value (5) is used");
        }
        elseif($window_size > 0 && is_int($window_size))
        {
            self::add_debug_msg(" * Threads set to:\t$window_size");
        }
        else
        {
            throw new AngryCurlException(" (!) Wrong threads amount in execute():\t$window_size");
        }
        
        self::add_debug_msg(" * Starting connections");
        
        $time_start = microtime(1);
        $result = parent::execute($window_size);
        $time_end = microtime(1);
        
        self::add_debug_msg(" * Finished in ".round($time_end-$time_start,2)."s");
        
        return $result;
    }
    
    public function flush_requests()
    {
        $this->__set('requests', array());
    }

    public function load_useragent_list($input)
    {
        self::add_debug_msg("# Start loading useragent list");
        
        if(is_array($input))
        {
            $this->array_useragent = $input;
        }
        else
        {        
            $this->array_useragent = $this->load_from_file($input);
        }
        
        $this->n_useragent = count($this->array_useragent);
        
        if($this->n_useragent > 0)
        {
            self::add_debug_msg("# Loaded useragents:\t{$this->n_useragent}");
        }
        else
        {
            throw new AngryCurlException("# (!) No useragents loaded");
        }
        
        $this->use_useragent_list = true;
        
        return $this->n_useragent;
    }

    public function load_proxy_list($input, $window_size = 5, $proxy_type = 'http', $proxy_test_url = 'http://google.com', $proxy_valid_regexp = null)
    {
        self::add_debug_msg("# Start loading proxies");
        
        if(is_array($input))
        {
            $this->array_proxy = $input;
        }
        else
        {
            $this->array_proxy = $this->load_from_file($input);
        }        
        
        if( intval($window_size) < 1 || !is_int($window_size) )
        {
            throw new AngryCurlException(" (!) Wrong threads amount in load_proxy_list():\t$window_size");
        }

        
        if($proxy_type == 'socks5')
        {
            self::add_debug_msg(" * Proxy type set to:\tSOCKS5");
            $this->__set('options', array(CURLOPT_PROXYTYPE => CURLPROXY_SOCKS5));
        }
        else
        {
            self::add_debug_msg(" * Proxy type set to:\tHTTP");
        }
            
        $this->n_proxy = count($this->array_proxy);
        self::add_debug_msg(" * Loaded proxies:\t{$this->n_proxy}");
        
        if($this->n_proxy>0)
        {
            $n_dup = count($this->array_proxy);
            $this->array_proxy = array_values( array_unique( $this->array_proxy) );
            $n_dup -= count($this->array_proxy);
            
            self::add_debug_msg(" * Removed duplicates:\t{$n_dup}");
            unset($n_dup);
            
            $this->n_proxy = count($this->array_proxy);
            self::add_debug_msg(" * Unique proxies:\t{$this->n_proxy}");
            
            $this->proxy_test_url = $proxy_test_url;
            self::add_debug_msg(" * Proxy test URL:\t{$this->proxy_test_url}");
            
            if( !empty($proxy_valid_regexp) )
            {
                self::$proxy_valid_regexp = $proxy_valid_regexp;
                self::add_debug_msg(" * Proxy test RegExp:\t".self::$proxy_valid_regexp);
            }
            
            $this->filter_alive_proxy($window_size); 
        }
        else
        {
            throw new AngryCurlException(" (!) Proxies amount < 0 in load_proxy_list():\t{$this->n_proxy}");
        }
        
        $this->use_proxy_list = true;   
    }

    public static function callback_proxy_check($response, $info, $request)
    {
        static $rid = 0;
        $rid++;
    
        if($info['http_code']!==200)
        {
            self::add_debug_msg("   $rid->\t".$request->options[CURLOPT_PROXY]."\tFAILED\t".$info['http_code']."\t".$info['total_time']."\t".$info['url']);
            return;
        }

        if(!empty(self::$proxy_valid_regexp) && !@preg_match('#'.self::$proxy_valid_regexp.'#', $response) )
        {
            self::add_debug_msg("   $rid->\t".$request->options[CURLOPT_PROXY]."\tFAILED\tRegExp match:\t".self::$proxy_valid_regexp."\t".$info['url']);
            return;
        }
            self::add_debug_msg("   $rid->\t".$request->options[CURLOPT_PROXY]."\tOK\t".$info['http_code']."\t".$info['total_time']."\t".$info['url']);
            self::$array_alive_proxy[] = $request->options[CURLOPT_PROXY];
    }
    
    protected function filter_alive_proxy($window_size = 5)
    {
        self::add_debug_msg("# Start testing proxies");
        
        if( intval($window_size) < 1 || !is_int($window_size) )
        {
            throw new AngryCurlException(" (!) Wrong threads amount in filter_alive_proxy():\t$window_size");
        }
        
        $buff_callback_func = $this->__get('callback');
        $this->__set('callback',array('AngryCurl', 'callback_proxy_check'));

        foreach($this->array_proxy as $id => $proxy)
        {
            if( strlen($proxy) > 4)
                $this->request($this->proxy_test_url, $method = "GET", null, null, array(CURLOPT_PROXY => $proxy) );
        }

        $this->execute($window_size);
        
        $this->__set('requests', array());

        self::add_debug_msg("# Alive proxies:\t".count(self::$array_alive_proxy)."/".$this->n_proxy);
        
        $this->n_proxy = count(self::$array_alive_proxy);
        $this->array_proxy = self::$array_alive_proxy;
        $this->__set('callback', $buff_callback_func);
    }

    protected function load_from_file($filename, $delim = "\n")
    {
        $data;
        $fp = @fopen($filename, "r");
        
        if(!$fp)
        {
            self::add_debug_msg("(!) Failed to open file: $filename");
            return array();
        }
        
        $data = @fread($fp, filesize($filename) );
        fclose($fp);
        
        if(strlen($data)<1)
        {
            self::add_debug_msg("(!) Empty file: $filename");
            return array();
        }
        
        $array = explode($delim, $data);
        
        if(is_array($array) && count($array)>0)
        {
            foreach($array as $k => $v)
            {
                if(strlen( trim($v) ) > 0)
                    $array[$k] = trim($v);
            }
            return $array;
        }
        else
        {
            self::add_debug_msg("(!) Empty data array in file: $filename");
            return array();
        }
    }
    
    public static function print_debug()
    {
        echo "<pre>";
        echo htmlspecialchars( implode("\n", self::$debug_info) );
        echo "</pre>";
    }
    
    public static function add_debug_msg($msg)
    {
        if(self::$debug_log)
        {
            self::$debug_info[] = $msg;
        }
        
        if(self::$console_mode)
        {
            echo htmlspecialchars($msg)."\r\n";
        }
    }

    function __destruct()
    {
        self::add_debug_msg("# Finishing ...");
        parent::__destruct();
    } 
}

class AngryCurlException extends Exception
{
    public function __construct($message = "", $code = 0)
    {
        AngryCurl::add_debug_msg($message);
        parent::__construct($message, $code);
    }
}

class AngryCurlRequest extends RollingCurlRequest
{
    
}

class RollingCurlRequest {
    public $url = false;
    public $method = 'GET';
    public $post_data = null;
    public $headers = null;
    public $options = null;

    function __construct($url, $method = "GET", $post_data = null, $headers = null, $options = null) {
        $this->url = $url;
        $this->method = $method;
        $this->post_data = $post_data;
        $this->headers = $headers;
        $this->options = $options;
    }

    public function __destruct() {
        unset($this->url, $this->method, $this->post_data, $this->headers, $this->options);
    }
}

class RollingCurlException extends Exception {
}

class RollingCurl {

    private $window_size = 5;

    private $timeout = 5;

    private $callback;

    public $options = array(
        CURLOPT_SSL_VERIFYPEER	=> 1,
        CURLOPT_RETURNTRANSFER	=> 1,
        CURLOPT_CONNECTTIMEOUT	=> 3,
        CURLOPT_TIMEOUT			=> 5,
    );

    private $headers = array();
    private $requests = array();
    private $requestMap = array();

    function __construct($callback = null) {
        $this->callback = $callback;
    }

    public function __get($name) {
        return (isset($this->{$name})) ? $this->{$name} : null;
    }

    public function __set($name, $value) {
        if ($name == "options" || $name == "headers") {
            $this->{$name} = $value + $this->{$name};
        } else {
            $this->{$name} = $value;
        }
        return true;
    }

    public function add($request) {
        $this->requests[] = $request;
        return true;
    }

    public function request($url, $method = "GET", $post_data = null, $headers = null, $options = null) {
        $this->requests[] = new RollingCurlRequest($url, $method, $post_data, $headers, $options);
        return true;
    }

    public function get($url, $headers = null, $options = null) {
        return $this->request($url, "GET", null, $headers, $options);
    }

    public function post($url, $post_data = null, $headers = null, $options = null) {
        return $this->request($url, "POST", $post_data, $headers, $options);
    }

    public function execute($window_size = null) {
        if (sizeof($this->requests) == 1) {
            return $this->single_curl();
        } else {
            return $this->rolling_curl($window_size);
        }
    }

    private function single_curl() {
        $ch = curl_init();
        $request = array_shift($this->requests);
        $options = $this->get_options($request);
        curl_setopt_array($ch, $options);
        $output = curl_exec($ch);
        $info = curl_getinfo($ch);

        if ($this->callback) {
            $callback = $this->callback;
            if (is_callable($this->callback)) {
                // call_user_func($callback, $output, $info, $request);
				if( is_array( $callback ) )
					call_user_func_array($callback, [ $output, $info, $request ]);
				else
					call_user_func($callback, $output, $info, $request);
            }
        }
        else
            return $output;
        return true;
    }

    private function rolling_curl($window_size = null) {
        if ($window_size)
            $this->window_size = $window_size;

        if (sizeof($this->requests) < $this->window_size)
            $this->window_size = sizeof($this->requests);

        if ($this->window_size < 2) {
            throw new RollingCurlException("Window size must be greater than 1");
        }

        $master = curl_multi_init();

        for ($i = 0; $i < $this->window_size; $i++) {
            $ch = curl_init();

            $options = $this->get_options($this->requests[$i]);

            curl_setopt_array($ch, $options);
            curl_multi_add_handle($master, $ch);

            $key = (string) $ch;
            $this->requestMap[$key] = $i;
        }

        do {
            while (($execrun = curl_multi_exec($master, $running)) == CURLM_CALL_MULTI_PERFORM) ;
            if ($execrun != CURLM_OK)
                break;
            while ($done = curl_multi_info_read($master)) {

                $info = curl_getinfo($done['handle']);
                $output = curl_multi_getcontent($done['handle']);

                $callback = $this->callback;
                if (is_callable($callback)) {
                    $key = (string) $done['handle'];
                    $request = $this->requests[$this->requestMap[$key]];
                    unset($this->requestMap[$key]);
                    // call_user_func($callback, $output, $info, $request);
					if( is_array( $callback ) )
						call_user_func_array($callback, [ $output, $info, $request ]);
					else
						call_user_func($callback, $output, $info, $request);
                }

                if ($i < sizeof($this->requests) && isset($this->requests[$i]) && $i < count($this->requests)) {
                    $ch = curl_init();
                    $options = $this->get_options($this->requests[$i]);
                    curl_setopt_array($ch, $options);
                    curl_multi_add_handle($master, $ch);

                    $key = (string) $ch;
                    $this->requestMap[$key] = $i;
                    $i++;
                }

                curl_multi_remove_handle($master, $done['handle']);

            }

            if ($running)
                curl_multi_select($master, $this->timeout);

        } while ($running);
        curl_multi_close($master);
        return true;
    }

    private function get_options($request) {
        $options = $this->__get('options');
        if (ini_get('safe_mode') == 'Off' || !ini_get('safe_mode')) {
            $options[CURLOPT_FOLLOWLOCATION] = 1;
            $options[CURLOPT_MAXREDIRS] = 5;
        }
        $headers = $this->__get('headers');

        if ($request->options) {
            $options = $request->options + $options;
        }

        $options[CURLOPT_URL] = $request->url;

        if ($request->post_data) {
            $options[CURLOPT_POST] = 1;
            $options[CURLOPT_POSTFIELDS] = $request->post_data;
        }
        if ($headers) {
            $options[CURLOPT_HEADER] = 0;
            $options[CURLOPT_HTTPHEADER] = $headers;
        }

        return $options;
    }

    public function __destruct() {
        unset($this->window_size, $this->callback, $this->options, $this->headers, $this->requests);
    }
}
