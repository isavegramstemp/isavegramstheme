<?php 
	get_header(); 
/* 		
	$args = [ 
				'post_type'			=> 'post', 
				'post_status'		=> 'publish', 
				'order'				=> 'DESC', 
				// 'meta_key'			=> 'edge_followed_by',
				'meta_key'			=> 'storys_data',
				'orderby'			=> 'meta_value', 
				'posts_per_page'	=> 10,
			];
 */		
	$args = [
				'post_type'			=> 'post', 
				'post_status'		=> 'publish', 
				// 'orderby'			=> 'rand', 
				'orderby'			=> 'meta_value',
				'order'				=> 'DESC',
				'meta_key'			=> 'edge_followed_by',
				'posts_per_page'	=> 25,
			];
	$users = get_posts( $args );

?>

		<div class="section_main">
			<div class="container">
				<div class="row">
					<div class="col-xl-7 col-lg-9" id="load-more-data">
						<div class="title_min hidden_mob">Photo / Videos</div>
						
						<?php get_gallery(); ?>
						
					</div>
					
					<div class="col-xl-3 col-lg-3 stories_block">
						<div class="title_min">Stories</div>
						<div class="col_stories">
							
							<?php if( $users ){ ?>
								<div class="scroll_stories">
								
									<?php foreach( $users as $user ){ 
										$storys_data = get_post_meta($user->ID, 'storys_data', true);
									?>
										<div class="item_story" data-item='<?=json_encode( $storys_data ) ?>'>
											<a href="#photos_modal1" class="image_story fancybox">
												<img src="<?=get_post_meta( $user->ID, 'profile_pic_url', true )?>" alt="<?=$user->post_title ?>">
											</a>
											<div>
												<a href="<?=get_the_permalink( $user->ID ) ?>" class="name_story"><?=$user->post_title ?></a>
												<!-- <div class="time_story">1 minute ago</div> -->
											</div>
										</div>
									<?php } ?>
										
								</div>
							<?php } ?>
								
								
								
						</div>
					</div>
					
					<div class="col-12 od_2">
						<div class="align_center">
							<a href="#" class="link_main link_more" id="load-more">More</a>
						</div>
						<div class="line"></div>
					</div>
					
					<div class="col-xl-2 col-12 col_hashtags">
						<div class="title_min">Hashtags</div>
						<?php if($tags = get_terms('tags', [ 'hide_empty' => true, 'number' => 60, 'orderby' => 'count', 'order' => 'DESC' ])){ ?>
							<div class="wrap_hashtags">
								<?php foreach($tags as $tag){ ?>
									<a href="<?=get_tag_link( $tag->term_id )?>" class="link_main"><?=$tag->name?></a>
								<?php } ?>
								
							</div>
						<?php } ?>
						
						<div class="align_center hidden_desktop">
							<a href="#" class="link_main link_more" id="load-more">More</a>
						</div>
					</div>
				</div>
			</div>
		</div>

<?php get_footer(); ?>