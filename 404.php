<?php get_header(); ?>
		
	<div class="section_error">
		<div class="container">
			<div class="image_error">
				<img src="<?php bloginfo('template_url'); ?>/img/404.svg" alt="alt">
			</div>
			<div class="title_error">Page not found</div>
			<div class="descr_error">Oops, looks like the page you have requested no found</div>
			<a href="/" class="btn_main">Go back home</a>
		</div>
	</div>

<?php get_footer(); ?>