<?php
	
	$args = [
					'post_type'			=> 'gallery',
					'posts_per_page'	=> 21,
					'post_status'		=> 'publish',
					// 'meta_query'		=> [
											// 'relation' => 'OR',
												// [
													// 'key'			=> 'thumbnail_resources',
													// 'compare'		=> 'EXISTS',
												// ],
												// [
													// 'key'			=> 'edge_sidecar_to_children',
													// 'compare'		=> 'EXISTS',
												// ],
										// ]
			];
	
	if( is_single() ){
		global $post;
		
		$args['order'] 			= 'desc';
		$args['orderby'] 		= 'post_date';
		$args['post_parent']	= $post->ID;
		
	} elseif( is_tax() ){
		global $wp_query;
		
		 $args['tax_query']		= [
									[
										'taxonomy' => 'tags',
										'field'    => 'id',
										'terms'    => [ $wp_query->get_queried_object_id() ]
									]
								];
	
	} else {
		$args['orderby']		= 'rand';
	}
		
	
	$query = new WP_Query;
	$posts = $query->query( $args );
		
	// echo "<pre>"; 
		// print_r( $posts ); 
	// echo "</pre>"; 

?>
<?php if( $posts ){ ?>
	

		<div class="gallery hidden_mob">
		
			<div class="col_big">
				<?php if( isset( $posts[0] ) ){ $item = _get_meta( $posts[0] ); ?>
					<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?>" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[2]->src; ?>);">
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
						<a href="<?=get_the_permalink( $posts[0]->ID ) ?>" class="name_item"><?=get_the_title( $posts[0]->post_parent )?></a>
					</div>
				<?php } ?>
			</div>
			
			<div class="col_middle">
			
				<?php if( isset( $posts[1] ) ){ $item = _get_meta( $posts[1] ); ?>
					<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?> item_middle" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[0]->src; ?>);">
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[1]->post_parent )?></a>
					</div>
				<?php } ?>
				
				<?php if( isset( $posts[2] ) ){ $item = _get_meta( $posts[2] ); ?>
					<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?> item_middle" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[0]->src; ?>);">
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[2]->post_parent )?></a>
					</div>
				<?php } ?>
				
				<?php if( isset( $posts[3] ) ){ $item = _get_meta( $posts[3] ); ?>
					<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?>" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[2]->src; ?>);">
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[3]->post_parent )?></a>
					</div>
				<?php } ?>
			</div>
			
			<div class="col_middle">
			
				<?php if( isset( $posts[4] ) ){ $item = _get_meta( $posts[4] ); ?>
					<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?>" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[2]->src; ?>);">
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[4]->post_parent )?></a>
					</div>
				<?php } ?>
				
				<?php if( isset( $posts[5] ) ){ $item = _get_meta( $posts[5] ); ?>
					<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?> item_middle" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[0]->src; ?>);">
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[5]->post_parent )?></a>
					</div>
				<?php } ?>
				
				<?php if( isset( $posts[6] ) ){ $item = _get_meta( $posts[6] ); ?>
					<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?> item_middle" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[0]->src; ?>);">
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[6]->post_parent )?></a>
					</div>
				<?php } ?>
				
			</div>
			
			<div class="col_middle">
			
				<?php if( isset( $posts[7] ) ){ $item = _get_meta( $posts[7] ); ?>
					<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?>" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[2]->src; ?>);">
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[7]->post_parent )?></a>
					</div>
				<?php } ?>
					
				<?php if( isset( $posts[8] ) ){ $item = _get_meta( $posts[8] ); ?>
					<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?> item_middle" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[0]->src; ?>);">
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[8]->post_parent )?></a>
					</div>
				<?php } ?>
				
				<?php if( isset( $posts[9] ) ){ $item = _get_meta( $posts[9] ); ?>
					<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?> item_middle" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[0]->src; ?>);">
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[9]->post_parent )?></a>
					</div>
				<?php } ?>
				
			</div>
			
			<div class="col_middle">
				
				<?php if( isset( $posts[10] ) ){ $item = _get_meta( $posts[10] ); ?>
					<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?>" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[1]->src; ?>);">
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[10]->post_parent )?></a>
					</div>
				<?php } ?>
					
				<?php if( isset( $posts[11] ) ){ $item = _get_meta( $posts[11] ); ?>
					<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?> item_middle" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[0]->src; ?>);">
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[11]->post_parent )?></a>
					</div>
				<?php } ?>
				
				<?php if( isset( $posts[12] ) ){ $item = _get_meta( $posts[12] ); ?>
					<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?> item_middle" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[0]->src; ?>);">
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[12]->post_parent )?></a>
					</div>
				<?php } ?>
				
			</div>
			
			<div class="col_big">
				<?php if( isset( $posts[13] ) ){ $item = _get_meta( $posts[13] ); ?>
				<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?>" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[2]->src; ?>);">
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[13]->post_parent )?></a>
				</div>
				<?php } ?>
			</div>
			
			<div class="col_big">
				<?php if( isset( $posts[14] ) ){ $item = _get_meta( $posts[14] ); ?>
				<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?>" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[2]->src; ?>);">
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[14]->post_parent )?></a>
				</div>
				<?php } ?>
			</div>
			
			<div class="col_middle">	
			
				<?php if( isset( $posts[15] ) ){ $item = _get_meta( $posts[15] ); ?>
				<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?> item_middle" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[0]->src; ?>);">
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[15]->post_parent )?></a>
				</div>
				<?php } ?>
				
				<?php if( isset( $posts[16] ) ){ $item = _get_meta( $posts[16] ); ?>
				<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?> item_middle" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[0]->src; ?>);">
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[16]->post_parent )?></a>
				</div>
				<?php } ?>
				
				<?php if( isset( $posts[17] ) ){ $item = _get_meta( $posts[17] ); ?>
				<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?>" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[1]->src; ?>);">
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[17]->post_parent )?></a>
				</div>
				<?php } ?>
			</div>
			
			<div class="col_middle">
				
				<?php if( isset( $posts[18] ) ){ $item = _get_meta( $posts[18] ); ?>
				<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?>" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[1]->src; ?>);">
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[18]->post_parent )?></a>
				</div>
				<?php } ?>
				
				<?php if( isset( $posts[19] ) ){ $item = _get_meta( $posts[19] ); ?>
				<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?> item_middle" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[0]->src; ?>);">
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[19]->post_parent )?></a>
				</div>
				<?php } ?>
				
				<?php if( isset( $posts[20] ) ){ $item = _get_meta( $posts[20] ); ?>
				<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?> item_middle" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[0]->src; ?>);">
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[20]->post_parent )?></a>
				</div>
				<?php } ?>
			</div>
			
		</div>
		<div class="gallery show_mob">
			
			<div class="col_middle">
				
				<?php if( isset( $posts[0] ) ){ $item = _get_meta( $posts[0] ); ?>
				<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?>" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[1]->src; ?>);">
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[0]->post_parent )?></a>
				</div>
				<?php } ?>
				
				<?php if( isset( $posts[1] ) ){ $item = _get_meta( $posts[1] ); ?>
				<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?>" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[1]->src; ?>);">
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[1]->post_parent )?></a>
				</div>
				<?php } ?>
				
			</div>
			
			<div class="col_big">
				<?php if( isset( $posts[2] ) ){ $item = _get_meta( $posts[2] ); ?>
				<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?>" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[2]->src; ?>);">
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[2]->post_parent )?></a>
				</div>
				<?php } ?>
			</div>
			
			<div class="col_middle">
			
				<?php if( isset( $posts[3] ) ){ $item = _get_meta( $posts[3] ); ?>
				<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?>" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[1]->src; ?>);">
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[3]->post_parent )?></a>
				</div>
				<?php } ?>
				
				<?php if( isset( $posts[4] ) ){ $item = _get_meta( $posts[4] ); ?>
				<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?>" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[1]->src; ?>);">
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[4]->post_parent )?></a>
				</div>
				<?php } ?>
				
			</div>
			<div class="col_middle">
				
				<?php if( isset( $posts[5] ) ){ $item = _get_meta( $posts[5] ); ?>
				<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?>" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[1]->src; ?>);">
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[5]->post_parent )?></a>
				</div>
				<?php } ?>
				
				<?php if( isset( $posts[6] ) ){ $item = _get_meta( $posts[6] ); ?>
				<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?>" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[1]->src; ?>);">
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[6]->post_parent )?></a>
				</div>
				<?php } ?>
				
			</div>
			
			<div class="col_middle">
				
				<?php if( isset( $posts[7] ) ){ $item = _get_meta( $posts[7] ); ?>
				<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?>" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[1]->src; ?>);">
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[7]->post_parent )?></a>
				</div>
				<?php } ?>
				
				<?php if( isset( $posts[8] ) ){ $item = _get_meta( $posts[8] ); ?>
				<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?>" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[1]->src; ?>);">
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[8]->post_parent )?></a>
				</div>
				<?php } ?>
				
			</div>
			
			<div class="col_big">
				<?php if( isset( $posts[9] ) ){ $item = _get_meta( $posts[9] ); ?>
				<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?>" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[2]->src; ?>);">
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[9]->post_parent )?></a>
				</div>
				<?php } ?>
			</div>
			
			<div class="col_middle">
				<?php if( isset( $posts[10] ) ){ $item = _get_meta( $posts[10] ); ?>
				<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?>" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[1]->src; ?>);">
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[10]->post_parent )?></a>
				</div>
				<?php } ?>
				
				<?php if( isset( $posts[11] ) ){ $item = _get_meta( $posts[11] ); ?>
				<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?>" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[1]->src; ?>);">
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[11]->post_parent )?></a>
				</div>
				<?php } ?>
			</div>
			
			
			<div class="col_middle">
				<?php if( isset( $posts[12] ) ){ $item = _get_meta( $posts[12] ); ?>
				<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?>" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[1]->src; ?>);">
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[12]->post_parent )?></a>
				</div>
				<?php } ?>
				
				<?php if( isset( $posts[13] ) ){ $item = _get_meta( $posts[13] ); ?>
				<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?>" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[1]->src; ?>);">
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
					<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[13]->post_parent )?></a>
				</div>
				<?php } ?>
			</div>
			
			
			<div class="col_middle">
				<?php if( isset( $posts[14] ) ){ $item = _get_meta( $posts[14] ); ?>
					<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?>" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[1]->src; ?>);">
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[14]->post_parent )?></a>
					</div>
				<?php } ?>
				
				<?php if( isset( $posts[15] ) ){ $item = _get_meta( $posts[15] ); ?>
					<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?>" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[1]->src; ?>);">
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[15]->post_parent )?></a>
					</div>
				<?php } ?>
			</div>
			
			<div class="col_middle">
				<?php if( isset( $posts[16] ) ){ $item = _get_meta( $posts[16] ); ?>
					<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?>" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[1]->src; ?>);">
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[16]->post_parent )?></a>
					</div>
				<?php } ?>
				
				<?php if( isset( $posts[17] ) ){ $item = _get_meta( $posts[17] ); ?>
					<div class="item_gallery <?=isset( $item->video_url ) ? 'item_video' : ''?> <?=isset( $item->edge_sidecar_to_children ) ? 'item_many_photos' : ''?>" data-item='<?=json_encode( $item ) ?>' style="background-image: url(<?=$item->thumbnail_resources[1]->src; ?>);">
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="content_item fancybox"></a>
						<a href="<?=isset( $item->video_url ) ? '#video_modal1' : '#photos_modal1'?>" class="name_item"><?=get_the_title( $posts[17]->post_parent )?></a>
					</div>
				<?php } ?>
			</div>
			
		</div>

<?php } ?>