<div id="photos_modal1" class="modal_block">
		<div class="modal_wrap">
			<ul class="slider_modal">
				<li>
					<div class="item_modal">
						<div class="billbord_modal_mobile" style="background-image: url(<?php bloginfo('template_url'); ?>/img/billbord.jpg);">
							<span>AD</span>
						</div>
						<div class="header_modal">
							<div class="left_header_modal">
								<a href="#" class="avatar_modal">
									<img class="profile_pic_url" alt="alt">
								</a>
								<a href="#" class="name_item_result verify_item username" id="username"></a>
								<!-- <div class="descr_header_modal">Coron, Palawan</div> -->
							</div>
							<div class="right_header_modal">
								<ul class="rating_list">
									<li><div class="rating_item active"></div></li>
									<li><div class="rating_item active"></div></li>
									<li><div class="rating_item active"></div></li>
									<li><div class="rating_item active"></div></li>
									<li><div class="rating_item"></div></li>
								</ul>
								<div class="share_block">
									<p>Share this:</p>
									<ul class="share_list">
										<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/tg.svg" alt="alt"></a></li>
										<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/in.svg" alt="alt"></a></li>
										<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/tw.svg" alt="alt"></a></li>
										<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/fb.svg" alt="alt"></a></li>
										<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/wp.svg" alt="alt"></a></li>
									</ul>
								</div>
								<div class="tablet_head_right">
									<a href="#" class="btn_social"></a>
									<ul class="social_list_mob">
										<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/tg.svg" alt="alt"><span>Telegram</span></a></li>
										<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/in.svg" alt="alt"><span>LinkedIn</span></a></li>
										<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/tw.svg" alt="alt"><span>Twitter</span></a></li>
										<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/fb.svg" alt="alt"><span>Facebook</span></a></li>
										<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/wp.svg" alt="alt"><span>WhatsApp</span></a></li>
									</ul>
									<a href="#" class="download_btn_tablet"></a>
									<a href="#" class="close_btn_modal"></a>
								</div>
							</div>
						</div>
						<div class="d_flex">
							<ul class="slider_main"></ul>

							<?=get_field('banner_post', 'option') ?>
							
						</div>
						<div class="bottom_modal">
							<a href="#" class="image_story">
								<img class="profile_pic_url" alt="alt">
							</a>
							<a href="#" class="name_item_result verify_item username"></a>
							<p id="content"></p>
							<div class="time_modal time"></div>
						</div>
					</div>
				</li>
				<li>
					<div class="item_modal">
						<div class="billbord_modal_mobile" style="background-image: url(<?php bloginfo('template_url'); ?>/img/billbord2.jpg);">
							<span>AD</span>
						</div>
						<div class="header_modal">
							<div class="left_header_modal">
								<a href="#" class="avatar_modal">
									<img class="profile_pic_url" alt="alt">
								</a>
								<a href="#" class="name_item_result verify_item username"></a>
								<!-- <div class="descr_header_modal">Coron, Palawan</div> -->
							</div>
							<div class="right_header_modal">
								<ul class="rating_list">
									<li><div class="rating_item active"></div></li>
									<li><div class="rating_item active"></div></li>
									<li><div class="rating_item active"></div></li>
									<li><div class="rating_item active"></div></li>
									<li><div class="rating_item"></div></li>
								</ul>
								<div class="share_block">
									<p>Share this:</p>
									<ul class="share_list">
										<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/tg.svg" alt="alt"></a></li>
										<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/in.svg" alt="alt"></a></li>
										<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/tw.svg" alt="alt"></a></li>
										<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/fb.svg" alt="alt"></a></li>
										<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/wp.svg" alt="alt"></a></li>
									</ul>
								</div>
								<div class="tablet_head_right">
									<a href="#" class="btn_social"></a>
									<ul class="social_list_mob">
										<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/tg.svg" alt="alt"><span>Telegram</span></a></li>
										<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/in.svg" alt="alt"><span>LinkedIn</span></a></li>
										<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/tw.svg" alt="alt"><span>Twitter</span></a></li>
										<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/fb.svg" alt="alt"><span>Facebook</span></a></li>
										<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/wp.svg" alt="alt"><span>WhatsApp</span></a></li>
									</ul>
									<a href="#" class="download_btn_tablet"></a>
									<a href="#" class="close_btn_modal"></a>
								</div>
							</div>
						</div>
						<div class="d_flex">
							<ul class="slider_main">
								<li>
									<div class="item_slide">
										<img src="<?php bloginfo('template_url'); ?>/img/slide1.jpg" alt="alt">
										<a href="#" class="download_btn"></a>
									</div>
								</li>
							</ul>
							<?=get_field('banner_post', 'option') ?>
						</div>
						<div class="bottom_modal">
							<a href="#" class="image_story">
								<img class="profile_pic_url" alt="alt">
							</a>
							<a href="#" class="name_item_result verify_item username"></a>
							<div class="time_modal time"></div>
						</div>
					</div>
				</li>
			</ul>
			<div class="bottom_modal_mob">
				<a href="#" class="btn_search_tablet"></a>
				<ul class="rating_list">
					<li><div class="rating_item active"></div></li>
					<li><div class="rating_item active"></div></li>
					<li><div class="rating_item active"></div></li>
					<li><div class="rating_item active"></div></li>
					<li><div class="rating_item"></div></li>
				</ul>
				<a href="#" class="close_btn_mobile"></a>
			</div>
		</div>
	</div>