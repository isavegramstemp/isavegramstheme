<?php
/* утф-8 */

class RedInstaBot{

	/* 
	* var url to request
	*/
	private $api		= 'https://www.instagram.com';
	
	/* 
	* var url to request
	*/
	private $streams	= 200;
	
	/* 
	* var login to instagram
	*/
	private $login			= 'test';
	
	/* 
	* var password to instagram
	*/
	private $password		= 'test';
	
	/* 
	* array data to links
	*/
	private $profiles		= [];
	
	/* 
	* var dir to data proxy
	*/
	private  $proxyList;
	
	/* 
	* var dir to data check proxyes
	*/
	private  $proxyData;
	
	/* 
	* array data bad proxyes
	*/
	private  $badProxy		= [];
	
	/* 
	* var user agent list
	*/
	private  $useragent_list;
	
	/* 
	* array data to request
	*/
	private  $args;
	

	function __construct(){
		
		$this->args				= (object) $_REQUEST;
		$this->proxyList		= __DIR__ . '/tmp/proxyes.txt';
		$this->proxyData		= __DIR__ . '/tmp/data-proxyes.txt';
		$this->useragent_list	= __DIR__ . '/tmp/useragent_list.txt';
		
		add_action('init',							[$this, 'init']);
		
		add_action('wp_ajax_explore',				[$this, 'scan']);
		add_action('wp_ajax_nopriv_explore',		[$this, 'scan']);
		
		add_action('wp_ajax_scan_proff',			[$this, 'scan_proff']);
		add_action('wp_ajax_nopriv_scan_proff',		[$this, 'scan_proff']);
	
		add_action('wp_ajax_scan_post',				[$this, 'scan_post']);
		add_action('wp_ajax_nopriv_scan_post',		[$this, 'scan_post']);
		
		add_action('wp_ajax_scan_storys',			[$this, 'scan_storys']);
		add_action('wp_ajax_nopriv_scan_storys',	[$this, 'scan_storys']);
	
		add_action('wp_ajax_checkProxyes',			[$this, 'checkProxyes']);
		add_action('wp_ajax_nopriv_checkProxyes',	[$this, 'checkProxyes']);
		
		/* tmp func */
		add_action('wp_ajax_test',					[$this, 'test']);
		add_action('wp_ajax_nopriv_test',			[$this, 'test']);
		
	
	}
	
	
	/* tmp func */
	public function test(){
		
			$args = [
					'post_type'			=> 'gallery',
					'posts_per_page'	=> -1,
					'post_status'		=> 'publish',
					'meta_query'		=> [
											'relation' => 'AND',
												[
													'key'			=> 'is_video',
													'compare'		=> 'EXISTS',
												],
												[
													'key'			=> 'video_url',
													'compare'		=> '=',
													'value'			=> '',
												],
										]
			];
	
		$query = new WP_Query;
		$posts = $query->query( $args );
		
		foreach( $posts as $post ){
			
			wp_update_post( [ 'ID' => $post->ID, 'post_status' => 'pending' ] );
		
		}
		
		
		echo "<pre>"; 
			print_r( $posts ); 
		echo "</pre>"; 

	
	}
	
	
	public function init(){
		
		$args = [
					'public'				=> true,
					'has_archive'			=> false,
					'show_ui'				=> true,
					'exclude_from_search'	=> true,
					'show_in_admin_bar'		=> false,
					'show_in_nav_menus'		=> false,
					'rewrite'				=> [ 'slug' => 'p' ],
				];
		register_post_type( 'gallery', $args );
		
		register_taxonomy( 'tags', ['gallery'], [] );
		
		$args = [
					'public'				=> false,
					'has_archive'			=> false,
					'show_ui'				=> false,
					'exclude_from_search'	=> false,
					'show_in_admin_bar'		=> false,
					'show_in_nav_menus'		=> false,
				];
		register_post_type( 'story', $args );
		
	}
	
	public function scan(){
		
		$response = $this->curl('https://www.instagram.com/graphql/query/?query_hash=ecd67af449fb6edab7c69a205413bfa7&variables={"first":50}');
		
		if( $data = json_decode( $response ) ){
			foreach( $data->data->user->edge_web_discover_media->edges as $item ){
				
				$this->profiles[ $item->node->shortcode ] = (object) [
																		'shortcode'			=> $item->node->shortcode,
																		'url_shortcode'		=> 'https://www.instagram.com/graphql/query/?query_hash=6ff3f5c474a240353993056428fb851e&variables={"shortcode":"'. $item->node->shortcode .'","include_reel":true,"include_logged_out":false}',
																		'tags'				=> $this->get_tags( @ $item->node->edge_media_to_caption->edges[0]->node->text ),
																		'username'			=> null,
																		'profile_pic_url'	=> null,
																	];
			}
			$this->accounts();
		}
	
	}
	
	public function _scan(){
		$mCurl = new RollingCurl( [$this, 'explore'] );
		$mCurl->window_size = $this->streams;
		
		$headers = [
						'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
						'accept-language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7,uk;q=0.6',
						'user-agent: Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1',
					];
		
		$mCurl->options = [
							CURLOPT_RETURNTRANSFER	=> 1,
							CURLOPT_HTTPHEADER		=> $headers,
							CURLOPT_COOKIEFILE		=>	__DIR__ . '/tmp/cookie.txt',
							CURLOPT_COOKIEJAR		=>	__DIR__ . '/tmp/cookie.txt',
						];
		for( $i = 0; $i < 1; $i++ ){
			$mCurl->get('https://www.instagram.com/graphql/query/?query_hash=ecd67af449fb6edab7c69a205413bfa7&variables={"first":24}');
		}
		
		$mCurl->execute();
		
		$this->accounts();
	}
		
	public function explore( $response, $info, $request ){
	
		if( $data = json_decode( $response ) ){	 

			foreach( $data->data->user->edge_web_discover_media->edges as $item ){
				
				$this->profiles[ $item->node->shortcode ] = (object) [
																		'shortcode'			=> $item->node->shortcode,
																		'url_shortcode'		=> 'https://www.instagram.com/graphql/query/?query_hash=6ff3f5c474a240353993056428fb851e&variables={"shortcode":"'. $item->node->shortcode .'","include_reel":true,"include_logged_out":false}',
																		'tags'				=> $this->get_tags( @ $item->node->edge_media_to_caption->edges[0]->node->text ),
																		'username'			=> null,
																		'profile_pic_url'	=> null,
																	];
			}
		}
	}
	
	private function accounts(){
		
		if( ! empty( $this->profiles ) ){
			
			$mCurl = new RollingCurl( [$this, 'get_accounts'] );
			$mCurl->window_size = $this->streams;
			
			$headers = [
							'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
							'accept-language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7,uk;q=0.6',
							'user-agent: Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1',
						];
			
			$mCurl->options = [
								CURLOPT_RETURNTRANSFER	=> 1,
								CURLOPT_HTTPHEADER		=> $headers,
								// CURLOPT_COOKIEFILE		=>	__DIR__ . '/tmp/cookie.txt',
								// CURLOPT_COOKIEJAR		=>	__DIR__ . '/tmp/cookie.txt',
							];
			
			foreach( $this->profiles as $k => $profile ){
				$mCurl->get( $profile->url_shortcode );
			}
			
			$mCurl->execute();
			
		}

		$this->upAcc();
	
	}
	
	private function upAcc(){
		
		echo "<pre>"; 
			print_r( $this->profiles ); 
		echo "</pre>"; 

		
		if( ! empty( $this->profiles ) ){		
			foreach( $this->profiles as $profil ){
				
				if( isset( $profil->username ) ){
					if( ! $p_ID = post_exists( $profil->username ) ){
						$args = [
									'post_title'	=> wp_strip_all_tags( $profil->username ),
									'post_type'		=> 'post',
									// 'post_content'	=> $profil->username,
									'post_status'	=> 'pending',
									'post_category'	=> 0,
								];
								
						$p_ID = wp_insert_post( $args );
							
							
					}
					
					if( $p_ID ){
						update_post_meta($p_ID, 'profile_pic_url', $profil->profile_pic_url);
					}
				
				}
			}
		}
		
	}

	public function scan_proff(){
		
		$args = [ 	
					'post_type'			=> 'post', 
					'post_status'		=> 'any', 
					// 'posts_per_page'	=> -1,
					'posts_per_page'	=> 100,
					'meta_query'		=> [
												'relation'	=> 'OR',
												[
													'key'			=> 'last_up',
													'type'			=> 'SIGNED',
													'value'			=> time() - 60 * 60 * 24,
													'compare'		=> '<=',
												],
												[
													'key'			=> 'last_up',
													'compare'		=> 'NOT EXISTS',
												],

										],
				];
	
		if( $users = get_posts( $args ) ){
			
			$AC = new AngryCurl( [$this, 'parse'] );
			// $AC->init_console();
			
			$headers = [
							'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
							'accept-language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7,uk;q=0.6',
							'user-agent: Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1',
						];
			
			$AC->options = [
								CURLOPT_RETURNTRANSFER	=> 1,
								CURLOPT_HTTPHEADER		=> $headers,
								CURLOPT_RETURNTRANSFER	=> 1,
								CURLOPT_CONNECTTIMEOUT	=> 3,
								CURLOPT_TIMEOUT			=> 5,
							];
				
			$AC->load_proxy_list( $this->proxyList, 200, 'https', $this->api, '<title>[^a-zA-Z]*Instagram');
			$AC->load_useragent_list( $this->useragent_list);
		
			foreach( $users as $user ){
			/* !!! eror post title is not real link !!! */
				$AC->get( $this->api . '/'. $user->post_title .'/feed/' );
				// $AC->get( $this->api . '/'. $user->post_title .'/?__a=1' );
			
			}
	
			$AC->execute( $this->streams );
			unset( $AC );
		
		}
	}

	public function parse( $response, $info, $request ){
		
		echo "<pre>"; 
			print_r( $info ); 
			print_r( $request ); 
		echo "</pre>____________________________________________________________________________________________________"; 
		
		if( $info['http_code'] == 200 ){
			if( preg_match('/>window\._sharedData[^\{]*(\{[^<]*)[;]?<\/script>/isU', $response, $data) ){
				$data = json_decode( $data[1] );
				
				if( isset( $data->entry_data->ProfilePage ) ){
					if( $p_ID = post_exists( $data->entry_data->ProfilePage[0]->graphql->user->username ) ){
						
						$args = [
									'ID'			=> $p_ID,
									'post_status'	=> 'publish',
									'post_content'	=> $data->entry_data->ProfilePage[0]->graphql->user->biography,
								];
					
						wp_update_post( $args );
						
						
						update_post_meta($p_ID, 'last_up', time());
						update_post_meta($p_ID, 'full_name', $data->entry_data->ProfilePage[0]->graphql->user->full_name);
						update_post_meta($p_ID, 'posts_count', $data->entry_data->ProfilePage[0]->graphql->user->edge_owner_to_timeline_media->count);
						update_post_meta($p_ID, 'edge_follow', $data->entry_data->ProfilePage[0]->graphql->user->edge_follow->count);
						update_post_meta($p_ID, 'edge_followed_by', $data->entry_data->ProfilePage[0]->graphql->user->edge_followed_by->count);
						update_post_meta($p_ID, 'profile_pic_url', $data->entry_data->ProfilePage[0]->graphql->user->profile_pic_url);
						update_post_meta($p_ID, 'profile_pic_url_hd', $data->entry_data->ProfilePage[0]->graphql->user->profile_pic_url_hd);
						update_post_meta($p_ID, 'cache', $data);
						
						if( isset( $data->entry_data->ProfilePage[0]->graphql->user->edge_owner_to_timeline_media->edges ) ){
						
							foreach( $data->entry_data->ProfilePage[0]->graphql->user->edge_owner_to_timeline_media->edges as $item ){
								
								if( ! post_exists( $item->node->shortcode, '', '', 'gallery' ) ){
								
									$args = [
												'post_parent'	=> $p_ID,
												'post_title'	=> wp_strip_all_tags( $item->node->shortcode ),
												'post_type'		=> 'gallery',
												'post_date'		=> date("Y-m-d H:i:s", $item->node->taken_at_timestamp),
												'post_content'	=> $item->node->edge_media_to_caption->edges[0]->node->text,
												'post_status'	=> $item->node->is_video ? 'pending' : 'publish',
												'post_category'	=> 0,
											];
											
									if( $id = wp_insert_post( $args ) ){
										
										wp_set_post_terms( $id, $this->get_tags( $item->node->edge_media_to_caption->edges[0]->node->text ), 'tags', true );
										
										if( $item->node->is_video )
											update_post_meta($id, 'is_video', $item->node->is_video);
										
										update_post_meta($id, '__typename', $item->node->__typename);
										
										if( isset( $item->node->video_view_count ) )
											update_post_meta($id, 'video_view_count', $item->node->video_view_count);
										
										
										if( isset( $item->node->accessibility_caption ) )
											update_post_meta($id, 'accessibility_caption', $item->node->accessibility_caption);
										
										update_post_meta($id, 'display_url', $item->node->display_url);
										update_post_meta($id, 'thumbnail_src', $item->node->thumbnail_src);
										update_post_meta($id, 'edge_liked_by', $item->node->edge_liked_by);
										update_post_meta($id, 'edge_media_preview_like', $item->node->edge_media_preview_like);
										update_post_meta($id, 'edge_media_to_comment', $item->node->edge_media_to_comment);
										update_post_meta($id, 'thumbnail_resources', $item->node->thumbnail_resources);
									}
								}
							}
							
						}
					
					}
				}
				
			}
	
		}
	}
	
	public function scan_post(){
		
		$args = [ 	
					'post_type'			=> 'gallery', 
					'posts_per_page'	=> -1,
					// 'posts_per_page'	=> 50,
					'meta_query'		=> [
												'relation'	=> 'OR',
												[
													'key'			=> 'last_up',
													'type'			=> 'SIGNED',
													'value'			=> time() - 60 * 60 * 24,
													'compare'		=> '<=',
												],
												[
													'key'			=> 'last_up',
													'compare'		=> 'NOT EXISTS',
												],

										],
				];
	
		if( $posts = get_posts( $args ) ){
			
			$AC = new AngryCurl( [$this, 'parse_post'] );
			// $AC->init_console();
			
			$headers = [
							'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
							'accept-language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7,uk;q=0.6',
							'user-agent: Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1',
						];
			
			$AC->options = [
								CURLOPT_RETURNTRANSFER	=> 1,
								CURLOPT_HTTPHEADER		=> $headers,
								CURLOPT_RETURNTRANSFER	=> 1,
								CURLOPT_CONNECTTIMEOUT	=> 3,
								CURLOPT_TIMEOUT			=> 5,
							];
				
			$AC->load_proxy_list( $this->proxyList, 200, 'https', $this->api, '<title>[^a-zA-Z]*Instagram');
			$AC->load_useragent_list( $this->useragent_list);
		
			foreach( $posts as $post ){
			
				$AC->get( $this->api . '/p/'. $post->post_title .'/' );
			
			}
	
			$AC->execute( $this->streams );
			unset( $AC );
			
			// $this->clearProxy();
		}
	}
	
				
	public function scan_storys(){
		
		$args = [ 	
					'post_type'			=> 'post', 
					'post_status'		=> 'publish', 
					'posts_per_page'	=> 20,
					'order'				=> 'DESC', 
					'meta_key'			=> 'edge_followed_by',
					'orderby'			=> 'meta_value', 
					'meta_query'		=> [
											'relation'	=> 'AND',
											[
												'relation'	=> 'OR',
												[
													'key'			=> '_s_last_up',
													'type'			=> 'SIGNED',
													'value'			=> time() - 60 * 60 * 24,
													'compare'		=> '<=',
												],
												[
													'key'			=> '_s_last_up',
													'compare'		=> 'NOT EXISTS',
												],

											],
											[
												'key'			=> 'cache',
												'compare'		=> 'EXISTS',
											]
										],
				];
	
		if( $users = get_posts( $args ) ){
	
			$AC = new AngryCurl( [$this, 'parse_storys'] );
			
			$headers = [
							'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
							'accept-language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7,uk;q=0.6',
							'user-agent: Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1',
						];
			
			$AC->options = [
								CURLOPT_RETURNTRANSFER	=> 1,
								CURLOPT_HTTPHEADER		=> $headers,
								CURLOPT_RETURNTRANSFER	=> 1,
								CURLOPT_CONNECTTIMEOUT	=> 3,
								CURLOPT_TIMEOUT			=> 5,
								CURLOPT_COOKIEFILE		=>	__DIR__ . '/tmp/cookie.txt',
								CURLOPT_COOKIEJAR		=>	__DIR__ . '/tmp/cookie.txt',
							];
				
			$AC->load_proxy_list( $this->proxyList, 200, 'https', $this->api, '<title>[^a-zA-Z]*Instagram');
			$AC->load_useragent_list( $this->useragent_list);
		
			foreach( $users as $user ){
				$cache = get_post_meta($user->ID, 'cache', true);
				$AC->get('https://www.instagram.com/graphql/query/?query_hash=ba71ba2fcb5655e7e2f37b05aec0ff98&variables={"reel_ids":["'. $cache->entry_data->ProfilePage[0]->graphql->user->id .'"],"tag_names":[],"location_ids":[],"highlight_reel_ids":[],"precomposed_overlay":true,"show_story_viewer_list":true,"story_viewer_fetch_count":50,"story_viewer_cursor":"","stories_video_dash_manifest":false}&ID='. $user->ID);
			}
	
			$AC->execute( $this->streams );
			unset( $AC );
		
			
		}
	
	}
	
	public function parse_storys( $response, $info, $request ){
	
		echo "<pre>"; 
			print_r( $response ); 
			print_r( $info ); 
		echo "</pre>"; 
	
		if( $info['http_code'] == 200 ){
			if( $data = json_decode( $response ) ){
				
				if( preg_match('/&ID=([0-9]*)/i', $info['url'], $id) ){
				
					if( @ $data->data->reels_media )
						update_post_meta( $id[1], 'storys_data', $data );
					
					if( @ $data->status == 'ok' )
						update_post_meta( $id[1], '_s_last_up', time() );
				}

			}
		}
		
	}
	
	public function ______scan_storys(){

		$args = [ 	
					'post_type'			=> 'post', 
					'post_status'		=> 'publish', 
					'posts_per_page'	=> 1,
					'order'				=> 'DESC', 
					'meta_key'			=> 'edge_followed_by',
					'orderby'			=> 'meta_value', 
					'meta_query'		=> [
											'relation'	=> 'AND',
											[
												'relation'	=> 'OR',
												[
													'key'			=> '_s_last_up',
													'type'			=> 'SIGNED',
													'value'			=> time() - 60 * 60 * 24,
													'compare'		=> '<=',
												],
												[
													'key'			=> '_s_last_up',
													'compare'		=> 'NOT EXISTS',
												],

											],
											[
												'key'			=> 'cache',
												'compare'		=> 'EXISTS',
											]
										],
				];
	
		if( $users = get_posts( $args ) ){
			foreach( $users as $user ){
				
				$cache = get_post_meta($user->ID, 'cache', true);
				$response = $this->curl('https://www.instagram.com/graphql/query/?query_hash=ba71ba2fcb5655e7e2f37b05aec0ff98&variables={"reel_ids":["'. $cache->entry_data->ProfilePage[0]->graphql->user->id .'"],"tag_names":[],"location_ids":[],"highlight_reel_ids":[],"precomposed_overlay":true,"show_story_viewer_list":true,"story_viewer_fetch_count":50,"story_viewer_cursor":"","stories_video_dash_manifest":false}');
				
				if( $data = json_decode( $response ) ){
					// echo "<pre>"; 
						// print_r( $cache );
						// print_r( $response );
						// print_r( $user->ID ); 
						// print_r( $user->post_name ); 
						// print_r( $data ); 
					// echo "</pre>"; 
					
					if( @ $data->data->reels_media )
						update_post_meta( $user->ID, 'storys_data', $data );
					
					if( @ $data->status == 'ok' )
						update_post_meta( $user->ID, '_s_last_up', time() );
				}
			
			}
		}
	
	}
	
	public function ___scan_storys(){
		$args = [ 	
					'post_type'			=> 'post', 
					'post_status'		=> 'publish', 
					'posts_per_page'	=> 1,
					'meta_query'		=> [
											'relation'	=> 'AND',
											[
												'relation'	=> 'OR',
												[
													'key'			=> '_s_last_up',
													'type'			=> 'SIGNED',
													'value'			=> time() - 60 * 60 * 24,
													'compare'		=> '<=',
												],
												[
													'key'			=> '_s_last_up',
													'compare'		=> 'NOT EXISTS',
												],

											],
											[
												'key'			=> 'cache',
												'compare'		=> 'EXISTS',
											]
										],
				];
	
		if( $users = get_posts( $args ) ){
			foreach( $users as $user ){
				
				$cache = get_post_meta($user->ID, 'cache', true);
			
				echo $response = $this->curl('https://www.instagram.com/graphql/query/?query_hash=52a36e788a02a3c612742ed5146f1676&variables={"user_id":"'. $cache->entry_data->ProfilePage[0]->graphql->user->id .'","include_chaining":true,"include_reel":true,"include_suggested_users":false,"include_logged_out_extras":false,"include_highlight_reels":true,"include_related_profiles":false}');
				
				if( $data = json_decode( $response ) ){
					
					echo  $data->data->user->reel->user->username;
					echo '<br>';
					echo  $cache->entry_data->ProfilePage[0]->graphql->user->username;
					
					// if( $user_ID = post_exists( $cache->entry_data->ProfilePage[0]->graphql->user->username ) ){
						
						if( isset( $data->data->user->edge_highlight_reels->edges[0]->node->id ) ){
							$ids = [];
							
							foreach($data->data->user->edge_highlight_reels->edges as $item){
								$ids[] = $item->node->id;
							}
							
							// echo 'https://www.instagram.com/graphql/query/?query_hash=ba71ba2fcb5655e7e2f37b05aec0ff98&variables={"reel_ids":[],"tag_names":[],"location_ids":[],"highlight_reel_ids":["'. implode('", "', $ids) .'"],"precomposed_overlay":true,"show_story_viewer_list":true,"story_viewer_fetch_count":50,"story_viewer_cursor":"","stories_video_dash_manifest":false}';
							// echo '<br>';
							
							$headers[] = 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8';
							$headers[] = 'Cookie: ig_did=511447C5-DFE1-4380-AFA3-B794D557800E; csrftoken=iBxX4gaCgu2gM9ScbXpuAdkvLBWDhOhJ; mid=XgJwfAAEAAEOEkAYdhJ_9H7MAFtS; fbm_124024574287414=base_domain=.instagram.com; ds_user_id=4556452400; sessionid=4556452400%3A5riLlaUOyas5rV%3A4; shbid=17780; shbts=1581793534.885958; fbsr_124024574287414=pF_WRNqv6BB8LZKc8ZkGUzBh6oL127rB_iBFK30FTeE.eyJ1c2VyX2lkIjoiMTAwMDAxNzAxMTQ4NTAxIiwiY29kZSI6IkFRRGxrdXUwdnVZd1NRU3U2dTROM1NQbGhPZlNtVGpZMTJab3MydDV0bFdoZkhyLUdQVzU2ajI0b0puQ3N2N0NPV2tmTUg2d1dRQ21MSDNKMW5MZHk5Y1JqYkdmY1NfLVo1cVNpMVRjRWZTNEkyZ1g3MlFiQUloclh2QlVBOHhkZ2JhUjZjdmtjMHdHdEloLU45SmRQaHZ3Ym04ejdaOUc0T2ZLQ3kwc2FDVk5oSDVtZ29aRG4xOWxkQnZSZlhDMndDUEVMaVBXVFhwZEhBZWdSSEp2S1NndWVxWHF5SkRzSGlud0FtOHVTdVhhS0RsMmQ2QVBnSGtBeS0wU18tUERFTjlud2hDNl9CakM5U1dKU2FaMGU5NEI4a3FVX3hsYTRZVGNzcVlET29sTU13Vll1T3Fzd24wXy1IazRMaV81RWw1TXl4cFo4eW1jaTZLZkttVkpYNlJKIiwib2F1dGhfdG9rZW4iOiJFQUFCd3pMaXhuallCQU02WDg2STBya0h6MU1MblNsVm1LbUtqS0lpNkhIalJRWkFqWkIxM1JlUnc3Q1cyQ3pqZklhZ2pOc05aQzBXczJaQUZ5ckl3S0pKZng5dDJzZ1dKZXhWdEltWkF0Rjd0bk5kTXlNZVFqZ3ZrNHlwV0g5RE9WcE8ybk5sR013Z1pCSmU5Z1dXRVBoWkN4V1g3MnBYQklpMVFlMTVuZzE1WXVJTkxyb21UbGV6IiwiYWxnb3JpdGhtIjoiSE1BQy1TSEEyNTYiLCJpc3N1ZWRfYXQiOjE1Nzk4MTg3OTl9; rur=VLL; urlgen="{\"185.119.213.152\": 49223\054 \"185.119.214.99\": 49223\054 \"193.34.232.21\": 13213}:1j3qJZ:Q5NohkavgjvH_NZkTpPZ8X0UOEw"';
							$headers[] = 'Host: www.instagram.com';
							$headers[] = 'User-Agent: Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1';
							
							
							echo $storys = $this->curl('https://www.instagram.com/graphql/query/?query_hash=52a36e788a02a3c612742ed5146f1676&variables={"reel_ids":[],"tag_names":[],"location_ids":[],"highlight_reel_ids":["'. implode('", "', $ids) .'"],"precomposed_overlay":true,"show_story_viewer_list":true,"story_viewer_fetch_count":50,"story_viewer_cursor":"","stories_video_dash_manifest":false}');
							// echo $storys = $this->curl('https://www.instagram.com/graphql/query/?query_hash=e74d51c10ecc0fe6250a295b9bb9db74&variables={"reel_ids":[],"tag_names":[],"location_ids":[],"highlight_reel_ids":["'. implode('", "', $ids) .'"],"precomposed_overlay":true,"show_story_viewer_list":true,"story_viewer_fetch_count":50,"story_viewer_cursor":"","stories_video_dash_manifest":false}', [], $headers);
							
							if( $storys && strpos( $storys, 'Server Error') === false )
								update_post_meta( $user->ID, 'storys_data', json_decode( $storys ) );
					
								// echo "<pre>"; 
									// print_r( json_decode( $storys ) ); 
								// echo "</pre>"; 
								
						}
						update_post_meta( $user->ID, '_s_last_up', time() );
						// <script>location.reload()</script>
					// }
					
				}
				
			}
		}
	}
	
	public function parse_post( $response, $info, $request ){
	
		echo "<pre>"; 
			print_r( $info ); 
			print_r( $request ); 
		echo "</pre>____________________________________________________________________________________________________"; 
		
		if( $info['http_code'] == 200 ){
			if( preg_match('/>window\._sharedData[^\{]*(\{[^<]*)[;]?<\/script>/isU', $response, $data) ){
				$data = json_decode( $data[1] );
				
				if( isset( $data->entry_data->PostPage ) ){
					if( $post_ID = post_exists( $data->entry_data->PostPage[0]->graphql->shortcode_media->shortcode, '', '', 'gallery' ) ){
						
						update_post_meta($post_ID, 'last_up', time());
						
						// if( @ $data->entry_data->PostPage[0]->graphql->shortcode_media->edge_media_to_parent_comment->count > 0 ){
							// foreach( $data->entry_data->PostPage[0]->graphql->shortcode_media->edge_media_to_parent_comment->edges as $comment ){
								// $date = date("Y-m-d H:i:s", $comment->node->created_at);
								
								// if( ! comment_exists( $comment->node->owner->username, $date ) ){
									// $args = [
												// 'comment_post_ID'		=> $post_ID,
												// 'comment_author'		=> $comment->node->owner->username,
												// 'comment_content'		=> $comment->node->text,
												// 'user_id'				=> 1,
												// 'comment_date'			=> $date,
												// 'comment_approved'		=> 1,
												// 'comment_meta'			=> [ 	
																				// 'profile_pic_url'	=> $comment->node->owner->profile_pic_url,
																				// 'viewer_has_liked'	=> $comment->node->viewer_has_liked,
																				// 'edge_liked_by'		=> $comment->node->edge_liked_by->count,
																			// ],
											// ];
									// $comm_id = wp_insert_comment( $args );
								// }
							// }
						// }
						
						if( isset( $data->entry_data->PostPage[0]->graphql->shortcode_media->edge_sidecar_to_children ) ){
							$slides = [];
							
							foreach( $data->entry_data->PostPage[0]->graphql->shortcode_media->edge_sidecar_to_children->edges as $slide ){
								 $slides[] = $slide->node->display_resources;
							}
							
							update_post_meta($post_ID, 'edge_sidecar_to_children', $slides);
						}
						
						if( isset( $data->entry_data->PostPage[0]->graphql->shortcode_media->video_url ) ){
							update_post_meta($post_ID, 'video_url', $data->entry_data->PostPage[0]->graphql->shortcode_media->video_url);
							update_post_meta($post_ID, 'video_view_count', $data->entry_data->PostPage[0]->graphql->shortcode_media->video_view_count);
							
							wp_update_post( $post_ID, 'publish' );
						}
						
					
					}
				}

			
			}
		} else
			$this->badProxy[] = $request->options[10004];
	}
	
	public function get_accounts( $response, $info, $request ){
		
		// echo "<pre>";
			// print_r( $response ); 
			// print_r( $info ); 
		// echo "</pre>____________________________________________________________________________________________________"; 
		
		if( $info['http_code'] == 200 ){
			$data = json_decode( $response );
			
			if( preg_match('/[\'"]shortcode[\'"]:[\'"]([^"]*)[\'"]/i', $info['url'], $short) )
				$short = $short[1];
			
			$this->profiles[ $short ]->username = $data->data->shortcode_media->owner->reel->owner->username;
			$this->profiles[ $short ]->profile_pic_url = $data->data->shortcode_media->owner->reel->owner->profile_pic_url;
		}
	}

	private function get_tags( $str ){
		if( preg_match_all('/(#[a-zA-Z0-9_-]+)/i', $str, $tags) )
			return $tags[1];
		
		return [];
	}
	
	private function clearProxy(){
		if( ! empty( $this->badProxy ) ){
			$proxyes = file( $this->proxyList );
			$proxyes = array_diff( $this->badProxy, $proxyes );
			file_put_contents( $this->proxyList, implode("\n", $proxyes) );
		}
	}
	
	public function checkProxyes(){
		$this->getProxyes();
		
		$AC = new AngryCurl();
		$AC->__set('window_size', 200);
		$AC->load_proxy_list( $this->proxyData, 200, 'http', 'https://www.instagram.com/', '<title>[^a-zA-Z]*Instagram' );
		$AC->load_useragent_list( $this->useragent_list );
		// $AC->init_console();
		
		if( $AC->array_proxy ){
			file_put_contents( $this->proxyList, implode("\n", $AC->array_proxy) );
			
			// $this->filterProxy();
		}
	}
	
	private function filterProxy(){
		$AC = new AngryCurl();
		$AC->__set('window_size', 200);
		$AC->load_proxy_list( $this->proxyList, 200, 'http', 'https://www.instagram.com/', '<title>[^a-zA-Z]*Instagram' );
		$AC->load_useragent_list( $this->useragent_list );
		
		if( $AC->array_proxy )
			file_put_contents( $this->proxyList, implode("\n", array_unique( $AC->array_proxy ) ) );
		
		echo "<pre>"; 
			print_r( array_unique( $AC->array_proxy ) ); 
		echo "</pre>"; 		
	}
	
	private function getProxyes( $lists = [], $proxyes = [] ){
		
		$lists[] = $this->curl('https://github.com/clarketm/proxy-list/blob/master/proxy-list.txt');
		$lists[] = $this->curl('http://spys.me/proxy.txt');
		$lists[] = $this->curl('https://hidemy.name/api/proxylist.txt?out=plain&lang=ru&code=859191940911114');
		$lists[] = $this->curl('https://awmproxy.net/freeproxy_e4830c9784e7d9a.txt');
		
		if( ! empty( $lists ) ){
			foreach( $lists as $list ){
				if( preg_match_all('/[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}[:][0-9]*/i', $list, $data) )
					$proxyes = array_merge( $proxyes, $data[0]);
				
			}
			$proxyes = array_unique( $proxyes );
			file_put_contents($this->proxyData, implode("\n", $proxyes ));
		}
	}
	
	
	private function curl( $url, $args = [], $headers = [] ){

		// $headers = [
						// 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
						// 'accept-language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7,uk;q=0.6',
						// 'user-agent: Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1',
				// ];
		$headers = [
					'accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
					'Accept-Language: en-US,en;q=0.5',
					// 'Cookie: ig_did=511447C5-DFE1-4380-AFA3-B794D557800E; csrftoken=iBxX4gaCgu2gM9ScbXpuAdkvLBWDhOhJ; mid=XgJwfAAEAAEOEkAYdhJ_9H7MAFtS; fbm_124024574287414=base_domain=.instagram.com; shbid=17780; shbts=1580681614.3902566; ds_user_id=4556452400; sessionid=4556452400%3A5riLlaUOyas5rV%3A4; fbsr_124024574287414=pF_WRNqv6BB8LZKc8ZkGUzBh6oL127rB_iBFK30FTeE.eyJ1c2VyX2lkIjoiMTAwMDAxNzAxMTQ4NTAxIiwiY29kZSI6IkFRRGxrdXUwdnVZd1NRU3U2dTROM1NQbGhPZlNtVGpZMTJab3MydDV0bFdoZkhyLUdQVzU2ajI0b0puQ3N2N0NPV2tmTUg2d1dRQ21MSDNKMW5MZHk5Y1JqYkdmY1NfLVo1cVNpMVRjRWZTNEkyZ1g3MlFiQUloclh2QlVBOHhkZ2JhUjZjdmtjMHdHdEloLU45SmRQaHZ3Ym04ejdaOUc0T2ZLQ3kwc2FDVk5oSDVtZ29aRG4xOWxkQnZSZlhDMndDUEVMaVBXVFhwZEhBZWdSSEp2S1NndWVxWHF5SkRzSGlud0FtOHVTdVhhS0RsMmQ2QVBnSGtBeS0wU18tUERFTjlud2hDNl9CakM5U1dKU2FaMGU5NEI4a3FVX3hsYTRZVGNzcVlET29sTU13Vll1T3Fzd24wXy1IazRMaV81RWw1TXl4cFo4eW1jaTZLZkttVkpYNlJKIiwib2F1dGhfdG9rZW4iOiJFQUFCd3pMaXhuallCQU02WDg2STBya0h6MU1MblNsVm1LbUtqS0lpNkhIalJRWkFqWkIxM1JlUnc3Q1cyQ3pqZklhZ2pOc05aQzBXczJaQUZ5ckl3S0pKZng5dDJzZ1dKZXhWdEltWkF0Rjd0bk5kTXlNZVFqZ3ZrNHlwV0g5RE9WcE8ybk5sR013Z1pCSmU5Z1dXRVBoWkN4V1g3MnBYQklpMVFlMTVuZzE1WXVJTkxyb21UbGV6IiwiYWxnb3JpdGhtIjoiSE1BQy1TSEEyNTYiLCJpc3N1ZWRfYXQiOjE1Nzk4MTg3OTl9; rur=VLL; urlgen="{\"185.119.213.152\": 49223\054 \"185.119.214.99\": 49223}:1iz8HA:GLXtN85TO2wiZ7A7Xmkhwr7LYyI"',
					'user-agent: Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1',
				];

		if( $Curl = curl_init() ){
			curl_setopt_array(
				$Curl, 
				[
					CURLOPT_URL 			=> 	$url,
					CURLOPT_HEADER			=>	false,
					CURLOPT_HTTPHEADER		=>	$headers,
					CURLOPT_RETURNTRANSFER 	=> 	true,
					CURLOPT_FOLLOWLOCATION 	=> 	true,
					CURLOPT_COOKIEFILE		=>	__DIR__ . '/tmp/cookie.txt',
					CURLOPT_COOKIEJAR		=>	__DIR__ . '/tmp/cookie.txt',
					CURLOPT_SSL_VERIFYPEER	=>	false,
					CURLOPT_SSL_VERIFYHOST	=>	false,
					// CURLOPT_POST 			=>  true,
					// CURLOPT_POSTFIELDS		=> 	$args,
				]
			);
			
			$data = curl_exec($Curl);

			curl_close($Curl);
			
		}
		return $data;
	}





}












?>