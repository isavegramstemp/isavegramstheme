<?php 
	get_header(); 
		
	// $users = get_posts( [ 'post_type' => 'post', 'post_status' => 'publish', 'orderby' => 'rand', 'posts_per_page' => 10 ] );

?>
		
	<?php if (have_posts()) { ?>
		<?php while (have_posts()) { the_post(); ?>

			<div class="section_main">
				<div class="container">
					<div class="row">
						<div class="col-xl-7 col-lg-9" id="load-more-data">
							<div class="title_min hidden_mob">Photo / Videos</div>
							
							<?php get_gallery(); ?>
							
						</div>
						
						<div class="col-xl-3 col-lg-3 stories_block">
							<div class="title_min">Stories</div>
							<div class="col_stories">
								
								<?php if( $storys = get_post_meta($post->ID, 'storys_data', true ) ){ ?>
									<div class="scroll_stories">
									
										<?php foreach( $storys->data->reels_media[0]->items as $story ){ ?>
											<div class="item_story" data-item='<?=json_encode( $story ) ?>'>
												<a href="#photos_modal1" class="image_story fancybox">
													<img src="<?=$story->display_resources[0]->src?>">
												</a>
												<div>
													<a href="<?=get_the_permalink( $post->ID ) ?>" class="name_story"><?=$post->post_title ?></a>
													<!-- <div class="time_story">1 minute ago</div> -->
												</div>
											</div>
										<?php } ?>
											
									</div>
								<?php } ?>
									
									
									
							</div>
						</div>
						
						<div class="col-12 od_2">
							<div class="align_center">
								<a href="#" class="link_main link_more" id="load-more" data-type="user" data-user="<?=$post->ID?>">More</a>
							</div>
							<div class="line"></div>
						</div>
						
						<div class="col-xl-2 col-12 col_user">
							<div class="title_min">User</div>
							<div class="image_user">
								<img src="<?=get_post_meta($post->ID, 'profile_pic_url', true)?>" alt="alt">
							</div>
							<div class="info_user">
								<div class="name_user">@<?=$post->post_title ?></div>
								<div class="line"></div>
								<div class="stat_user">
									<p><span><?=get_post_meta($post->ID, 'posts_count', true)?></span> posts</p>
									<p><span>
										<?php 
											$edge_followed_by = get_post_meta($post->ID, 'edge_followed_by', true); 
											
											echo isset( $edge_followed_by->count ) ? $edge_followed_by->count : $edge_followed_by;
										?></span> followers</p>
									<p><span>
										<?php
											$edge_follow = get_post_meta($post->ID, 'edge_follow', true);
											
											echo isset( $edge_follow->count ) ? $edge_follow->count : $edge_follow;
										?>
										</span> following</p>
								</div>
								<div class="line"></div>
								<p><span>Suki Cat</span></p>
								<p><?php the_content() ?></p>
							</div>
						</div>
						
					</div>
				</div>
			</div>

			<div class="bottom_text hidden_tablet">
				<div class="container">
					<?=get_field('post_text_footer', $post->ID) ?>	
				</div>
			</div>
			
			
			
		<?php } ?>
	<?php } ?>
	
<?php get_footer(); ?>